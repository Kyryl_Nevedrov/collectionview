//
//  CollectionViewCell.swift
//  CollectionView
//
//  Created by Admin on 18.07.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    var interest: Interest! {
        didSet {
            UIUpdate()
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    private func UIUpdate() {
        label.text = interest.title
        imageView.image = interest.featuredImage
    }
}
